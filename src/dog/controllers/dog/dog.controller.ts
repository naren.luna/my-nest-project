import {
  Controller,
  Get,
  Param,
  Query,
  Body,
  Post,
  Patch,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiQuery } from '@nestjs/swagger';

import { DogService } from 'src/dog/services/dog/dog.service';
import { DogDto } from 'src/dog/dto/dog.dto';
import { CreateDogDto } from 'src/dog/dto/create-dog.dto';
import { UpdateDogDto } from 'src/dog/dto/update-dog.dto';

// @ApiTags('Dog');
@Controller('dog')
export class DogController {
  constructor(private dogService: DogService) {}
  @ApiQuery({
    name: 'breed',
    type: String,
    description: 'The breed of the dog',
    required: false,
  })
  @ApiQuery({
    name: 'age',
    type: Number,
    description: 'The age of the dog',
    required: false,
  })
  @ApiOkResponse({
    description: 'The dogs records',
    type: DogDto,
    isArray: true,
  })
  @Get(':dogId')
  findOne(@Param('dogId') dogId: number) {
    return this.dogService.findOneBy(dogId);
  }
  @Get()
  find(@Query('breed') breed: string, @Query('age') age: number) {
    if (breed || age) {
      return this.dogService.findOne(breed, age);
    }
    return this.dogService.findAll();
  }
  @Post()
  @UsePipes(ValidationPipe)
  create(@Body() createDogDto: CreateDogDto) {
    return this.dogService.create(createDogDto);
  }
  @Patch(':dogId')
  @UsePipes(ValidationPipe)
  update(@Body() updateDogDto: UpdateDogDto, @Param('dogId') dogId: number) {
    return this.dogService.update(updateDogDto, dogId);
  }
  @Delete(':dogId')
  @UsePipes(ValidationPipe)
  delete(@Param('dogId') dogId: number) {
    return this.dogService.delete(dogId);
  }
}
