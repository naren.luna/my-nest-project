import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Dog } from 'src/entities';
import { Repository } from 'typeorm';

import { CreateDogDto } from 'src/dog/dto/create-dog.dto';
import { UpdateDogDto } from 'src/dog/dto/update-dog.dto';

@Injectable()
export class DogService {
  constructor(
    @InjectRepository(Dog) private readonly dogRepository: Repository<Dog>,
  ) {}
  async findOneBy(dogId: number) {
    try {
      const dog = await this.dogRepository.findOne({ where: { id: dogId } });
      console.log(dog);
      return dog;
    } catch (error) {
      return error;
    }
  }
  async findOne(breed: string, age: number) {
    try {
      const dog = await this.dogRepository.findOne({
        where: [{ breed }, { age }],
      });
      return dog;
    } catch (error) {
      return error;
    }
  }
  async findAll() {
    try {
      const dogs = await this.dogRepository.find();
      return dogs;
    } catch (error) {
      return error;
    }
  }
  create(createDogDto: CreateDogDto) {
    const newDog = this.dogRepository.create(createDogDto);
    return this.dogRepository.save(newDog);
  }
  update(updateDogDto: UpdateDogDto, idDog: number) {
    return this.dogRepository.save({
      id: idDog,
      ...updateDogDto,
    });
  }
  delete(idDog: number) {
    return this.dogRepository.delete({ id: idDog });
  }
}
