export const DOGS = [
  {
    id: 1,
    breed: 'Pitbull',
    age: 4,
    color: 'brown',
  },
  {
    id: 2,
    breed: 'Poodle',
    age: 6,
    color: 'black',
  },
  {
    id: 3,
    breed: 'Husky',
    age: 4,
    color: 'white-grey',
  },
  {
    id: 4,
    breed: 'Bulldog',
    age: 4,
    color: 'black',
  },
  {
    id: 5,
    breed: 'Beagle',
    age: 1,
    color: 'brown-white',
  },
  {
    id: 6,
    breed: 'Pitbull',
    age: 10,
    color: 'white',
  },
];
